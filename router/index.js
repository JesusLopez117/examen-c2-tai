//Constantes necesarias
const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');

router.get('/', (req, res) =>{
    const params = {
        numCon: req.query.numCon,
        nombre: req.query.nombre,
        domicilio: req.query.domicilio,
        nivEstu: parseFloat(req.query.nivEstu),
        pago: parseFloat(req.query.pago),
        dias: parseInt(req.query.dias)
    };
    res.render('pago.html', params);
});

router.post('/', (req, res) =>{
    const params = {
        numCon: req.body.numCon,
        nombre: req.body.nombre,
        domicilio: req.body.domicilio,
        nivEstu: parseFloat(req.body.nivEstu),
        pago: parseFloat(req.body.pago),
        dias: parseInt(req.body.dias)
    };

    res.render('pago.html', params);
});

router.get('/mostrar', (req, res) =>{
    const params = {
        numCon: req.query.numCon,
        nombre: req.query.nombre,
        domicilio: req.query.domicilio,
        nivEstu: parseFloat(req.query.nivEstu),
        pago: parseFloat(req.query.pago),
        dias: parseInt(req.query.dias)
    };
    res.render('mostrar.html', params);
});

router.post('/mostrar', (req, res) =>{
    const params = {
        numCon: req.body.numCon,
        nombre: req.body.nombre,
        domicilio: req.body.domicilio,
        nivEstu: parseFloat(req.body.nivEstu),
        pago: parseFloat(req.body.pago),
        dias: parseInt(req.body.dias)
    };

    res.render('mostrar.html', params);
});

//Exportar el modulo
module.exports = router;