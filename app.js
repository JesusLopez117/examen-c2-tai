//Constantes necesarias
const http = require('http');
const express = require('express');
const app = express();
const bodyparser = require('body-parser');
const path = require('path');

//Indicar donde se encuentran las rutas, importar el modulo
const rutas = require('./router/index');

app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/public/'));
app.use(bodyparser.urlencoded({extended:true}));
app.engine('html', require('ejs').renderFile);

//Referencia para que utilize las rutas de la carpeta router
app.use(rutas);


//Indicar el puerto de salida
const puerto = 3005;
app.listen(puerto, () => {
    console.log(`Iniciando puerto ${puerto}`);
})